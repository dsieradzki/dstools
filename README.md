# DSTools
- [Arguments](#arguments)
- [Logger](#logger)
- [Serial port](#serial-port)

## Installation
```bash
cd <PLACE_FOR_MODULE>
git submodule add https://gitlab.com/dsieradzki/dstools.git

CMakeLists.txt (by default modules are disabled):
    SET(DSTOOLS_MODULE_ARGUMENTS TRUE)
    SET(DSTOOLS_MODULE_LOGGER TRUE)
    SET(DSTOOLS_MODULE_SERIAL TRUE)


add_subdirectory(<PATH_TO_SUBMODULE>/dstools)
include_directories(<PATH_TO_SUBMODULE>/dstools/main/include)

target_link_libraries(<YOUR_EXECUTABLE> dstools)
```
## Modules
### Arguments
```cpp
#include <arguments/Arguments.hpp>
#include <iostream>
int main(int argc, char **argv) {
	
	// ex. ./prog_name --someArg value
	
	dstools::Arguments arguments(argc, argv);

	if (arguments.is_present("--someArg")) {
		std::string arg_value = arguments.argument_value("--someArg");
		std::cout<<arg_value;
	}

	return 0;
}
```

### Logger
```cpp
#include <logger/LoggerProvider.hpp>
int main() {
	
	/**
	dstools::LoggerProvider::setup_logger(
		std::make_unique<dstools::logger::DateTimeFormatter>(), 
		std::make_unique<dstools::logger::ConsoleAppender>() OR std::make_unique<dstools::logger::FileAppender>(<FILE_NAME>) 
		);
	 */
	
	dstools::LoggerProvider::setup_logger();
	LOGGER->set_level(dstools::logger::level::LOG_ERROR);
	
	LOGGER->info("Sample message");
	LOGGER->warning("Sample message");
	LOGGER->debug("Sample message");
	LOGGER->error("Sample message");
	
	LOG_INFO({"Sample message", "Another message"});
	LOG_WARNING("Sample message");
	LOG_DEBUG("Sample message");
	LOG_ERROR("Sample message");
	
	return 0;
}

```

### Serial port
```cpp
#include <serial/SerialPort.hpp>
#include <serial/SerialPortBoost.hpp>
int main() {
	// SETUP
	dstools::SerialPort *serial_port = new dstools::SerialPortBoost();
	serial_port->set_device("/dev/device");
	serial_port->set_speed(9600);
	serial_port->set_parity(::boost::asio::serial_port_base::parity::type::none);
	serial_port->set_stop_bits(::boost::asio::serial_port_base::stop_bits::type::one);
	serial_port->set_character_size(8);
	
	serial_port->open();
	
	if (serial_port->is_open()) {
		// WRITE DATA
		dstools::byte some_byte = 0xFF;
		dstools::byte_array data = {some_byte, 0xFE, 0xAA};
		serial_port->write(data);
		
		// READ DATA
		dstools::byte_array received_data;
		serial_port->read(received_data, 10);
		
		serial_port->close();
	}
	
	delete serial_port;
	return 0;
}
```

