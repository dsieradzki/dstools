/**
 *
 *  Copyright (C) 2019  Damian Sieradzki <damian.sieradzki@hotmail.com>
 *
 *  This file is part of DSTools.
 *
 *  DSTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DSTools.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include <arguments/Arguments.hpp>
#include <iostream>

namespace dstools {


	dstools::Arguments::Arguments(int &argc, char *argv[]) noexcept(false) {
		if (argc <= 1) return;

		std::vector<std::string> args;
		for (int i = 1; i < argc; i++) {
			args.emplace_back(argv[i]);
		}

		for (unsigned int i = 0; i < args.size(); i++) {
			if (with_value_parameter(args, i)) {
				value_arguments.insert(std::pair<std::string, std::string>(args.at(i),      //key
				                                                           args.at(i + 1)));//value
				i++;
			} else if (is_flag_parameter(args, i)) {
				flag_arguments.emplace_back(args.at(i));
			} else if (is_command(args.at(0), i)) { //its command
				command = args.at(0);
			} else {
				throw ArgumentParseException();
			}

		}

	}

	bool dstools::Arguments::with_value_parameter(std::vector<std::string> args, unsigned int i) const {
		return is_param(args.at(i)) && args.size() > i + 1 && !is_param(args.at(i + 1));
	}

	bool dstools::Arguments::is_flag_parameter(std::vector<std::string> args, unsigned int i) const {
		return (is_param(args.at(i)) && args.size() <= i + 1) || // [i] is param and argument is last one
		       (is_param(args.at(i)) && args.size() > i + 1 &&
		        is_param(args.at(i + 1))); //[i] i param and not last and next is param
	}


	unsigned long dstools::Arguments::count() const {
		return value_arguments.size() + flag_arguments.size() + (command.empty() ? 0 : 1);
	}

	std::string dstools::Arguments::argument_value(const std::string &key, const std::string &default_value) const {
		return is_present(key) ? argument_value(key) : default_value;
	}

	std::string dstools::Arguments::argument_value(const std::string &key) const {
		auto iter = value_arguments.find(key);
		if (iter != value_arguments.end()) {
			return iter->second;
		}
		return "";
	}

	bool dstools::Arguments::empty() const {
		return this->count() == 0;
	}

	bool dstools::Arguments::is_present(const std::string &name) const {
		for (const std::string &item : flag_arguments) {
			if (item.find(name) != std::string::npos) {
				return true;
			}
		}
		return value_arguments.find(name) != value_arguments.end();
	}


	bool dstools::Arguments::is_param(const std::string &value) const {
		if (value.find(PARAM_PREFIX_2) != std::string::npos) {
			return value.size() > 2;
		} else if (value.find(PARAM_PREFIX) != std::string::npos) {
			return value.size() > 1;
		}
		return false;
	}

	std::string dstools::Arguments::get_command() const {
		return command;
	}

	bool dstools::Arguments::is_command(std::string &value, unsigned int i) {
		return i == 0 && value.find(PARAM_PREFIX) == std::string::npos;
	}

}
