/**
 *
 *  Copyright (C) 2019  Damian Sieradzki <damian.sieradzki@hotmail.com>
 *
 *  This file is part of DSTools.
 *
 *  DSTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DSTools.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include <logger/formatter/DateTimeFormatter.hpp>
#include <sstream>
#include <iomanip>


#define THIS_CLASS dstools::__class_name(__PRETTY_FUNCTION__)

namespace dstools {
	inline std::string __class_name(const std::string &pretty_name) {
		std::size_t first_index = pretty_name.find(' ');
		std::size_t last_index = pretty_name.find('(');
		if (first_index == std::string::npos || last_index == std::string::npos) return pretty_name;

		first_index++;
		return pretty_name.substr(first_index, last_index - first_index);
	}
}

std::string dstools::logger::DateTimeFormatter::format(const std::string &msg) {
	std::stringstream result;
	result << generate_date_time();
	result << " " << msg;
	return result.str();
}

std::string dstools::logger::DateTimeFormatter::generate_date_time() {
	auto time = std::time(nullptr);
	std::tm local_time = *std::localtime(&time);
	return date_to_string(local_time);
}

std::string dstools::logger::DateTimeFormatter::date_to_string(std::tm date_time) {
	char date_as_string[100];
	std::strftime(date_as_string, 100, DATE_TIME_FORMAT.c_str(), &date_time);
	return std::string(date_as_string);
}
