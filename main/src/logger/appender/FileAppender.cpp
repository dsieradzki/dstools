/**
 *
 *  Copyright (C) 2019  Damian Sieradzki <damian.sieradzki@hotmail.com>
 *
 *  This file is part of DSTools.
 *
 *  DSTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DSTools.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include <logger/appender/FileAppender.hpp>
#include <iostream>

dstools::logger::FileAppender::FileAppender(const std::string &p_log_file_name) : log_file_name(p_log_file_name) {
	log_file.open(log_file_name, std::ios::app);
	if (!log_file.is_open()) {
		std::cerr << "Cannot open/create log file";
	}
}

dstools::logger::FileAppender::~FileAppender() {
	if (log_file.is_open()) {
		log_file.close();
	}
}

void dstools::logger::FileAppender::append(const std::string &msg) {
	if (log_file.is_open()) {
		log_file << msg << std::endl;
		log_file.flush();
	} else {
		std::cerr<<"Cannot save log to file";
	}
}
