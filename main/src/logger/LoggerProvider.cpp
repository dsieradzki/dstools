/**
 *
 *  Copyright (C) 2019  Damian Sieradzki <damian.sieradzki@hotmail.com>
 *
 *  This file is part of DSTools.
 *
 *  DSTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DSTools.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include <logger/LoggerProvider.hpp>
#include "LoggerImpl.hpp"

std::unique_ptr<dstools::Logger> dstools::LoggerProvider::logger;

dstools::Logger *dstools::LoggerProvider::get_logger() {
	if (!logger) {
		setup_logger();
		logger->set_level(dstools::logger::level::LOG_WARNING);
	}
	return logger.get();
}

void dstools::LoggerProvider::set_level(dstools::logger::level level) {
	get_logger()->set_level(level);
}

void dstools::LoggerProvider::setup_logger(std::unique_ptr<dstools::logger::Formatter> formatter, std::unique_ptr<dstools::logger::Appender> appender) {
	std::vector<std::unique_ptr<logger::Appender>> l_appenders;
	l_appenders.emplace_back(std::move(appender));
	logger = std::unique_ptr<LoggerImpl>(new LoggerImpl(std::move(formatter), std::move(l_appenders)));
}

void dstools::LoggerProvider::setup_logger(std::unique_ptr<dstools::logger::Formatter> formatter, std::vector<std::unique_ptr<dstools::logger::Appender>> appenders) {
	logger = std::unique_ptr<LoggerImpl>(new LoggerImpl(std::move(formatter), std::move(appenders)));
}

