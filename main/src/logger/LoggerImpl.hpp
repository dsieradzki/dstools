/**
 *
 *  Copyright (C) 2019  Damian Sieradzki <damian.sieradzki@hotmail.com>
 *
 *  This file is part of DSTools.
 *
 *  DSTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DSTools.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#ifndef DSTOOLS_LOGGERIMPL_HPP
#define DSTOOLS_LOGGERIMPL_HPP

#include <logger/Logger.hpp>
#include <logger/appender/Appender.hpp>
#include <logger/formatter/Formatter.hpp>
#include <mutex>
#include <memory>
#include <vector>

namespace dstools {

	class LoggerImpl : public Logger {
	public:
		LoggerImpl(
				std::unique_ptr<logger::Formatter> formatter,
				std::vector<std::unique_ptr<logger::Appender>> appenders);

		void set_level(dstools::logger::level new_level) override;

		void debug(const std::string &message) override;
		void info(const std::string &message) override;
		void warning(const std::string &message) override;
		void error(const std::string &message) override;

		void debug(const std::initializer_list<std::string> &messages) override;
		void info(const std::initializer_list<std::string> &messages) override;
		void warning(const std::initializer_list<std::string> &messages) override;
		void error(const std::initializer_list<std::string> &messages) override;
		logger::level get_level() override;
	private:
		std::unique_ptr<logger::Formatter> formatter;
		std::vector<std::unique_ptr<logger::Appender>> appenders;
		std::mutex log_mutex;
		dstools::logger::level level;

		std::string with_prefix(const std::string &prefix, const std::vector<std::string> &messages);
	};
}


#endif
