#include <utility>

/**
 *
 *  Copyright (C) 2019  Damian Sieradzki <damian.sieradzki@hotmail.com>
 *
 *  This file is part of DSTools.
 *
 *  DSTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DSTools.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include <utility>
#include <sstream>

#include "LoggerImpl.hpp"

dstools::LoggerImpl::LoggerImpl(std::unique_ptr<dstools::logger::Formatter> formatter, std::vector<std::unique_ptr<dstools::logger::Appender>> appenders)
		: Logger(),
		  formatter(std::move(formatter)),
		  appenders(std::move(appenders)) {
}

void dstools::LoggerImpl::debug(const std::string &message) {
	this->debug({message});
}

void dstools::LoggerImpl::info(const std::string &message) {
	this->info({message});
}

void dstools::LoggerImpl::warning(const std::string &message) {
	this->warning({message});
}

void dstools::LoggerImpl::error(const std::string &message) {
	this->error({message});
}


void dstools::LoggerImpl::debug(const std::initializer_list<std::string> &messages) {
	std::lock_guard<std::mutex> guard(log_mutex);
	if (level == logger::level::LOG_DEBUG) {
		for (const auto &appender : appenders) {
			appender->append(
					formatter->format(
							with_prefix("[DEBUG]", messages)
					)
			);
		}
	}
}

void dstools::LoggerImpl::info(const std::initializer_list<std::string> &messages) {
	std::lock_guard<std::mutex> guard(log_mutex);

	if (level == logger::level::LOG_INFO || level == logger::level::LOG_DEBUG) {
		for (const auto &appender : appenders) {
			appender->append(
					formatter->format(
							with_prefix("[INFO]", messages)
					)
			);
		}
	}
}

void dstools::LoggerImpl::warning(const std::initializer_list<std::string> &messages) {
	std::lock_guard<std::mutex> guard(log_mutex);
	if (level == logger::level::LOG_INFO || level == logger::level::LOG_DEBUG || level == logger::level::LOG_WARNING) {
		for (const auto &appender : appenders) {
			appender->append(
					formatter->format(
							with_prefix("[WARNING]", messages)
					)
			);
		}
	}
}

void dstools::LoggerImpl::error(const std::initializer_list<std::string> &messages) {
	std::lock_guard<std::mutex> guard(log_mutex);
	for (const auto &appender : appenders) {
		appender->append(
				formatter->format(with_prefix("[ERROR]", messages))
		);
	}
}

void dstools::LoggerImpl::set_level(dstools::logger::level new_level) {
	this->level = new_level;
}

dstools::logger::level dstools::LoggerImpl::get_level() {
	return this->level;
}

std::string dstools::LoggerImpl::with_prefix(const std::string &prefix, const std::vector<std::string> &messages) {
	std::stringstream log_ss;

	log_ss << prefix << " ";
	for (const std::string &message : messages) {
		log_ss << message;
	}

	return log_ss.str();
}



