/**
 *
 *  Copyright (C) 2019  Damian Sieradzki <damian.sieradzki@hotmail.com>
 *
 *  This file is part of DSTools.
 *
 *  DSTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DSTools.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include <serial/SerialPortBoost.hpp>
#include <iostream>

dstools::SerialPortBoost::SerialPortBoost() :
		m_device_name(),
		m_speed(38400),
		m_serial(m_io),
		m_reader(m_serial, 3000) {

}


void dstools::SerialPortBoost::open() {
	std::lock_guard<std::mutex> guard_read(m_serial_port_read_mutex);
	std::lock_guard<std::mutex> guard_write(m_serial_port_write_mutex);
	m_serial.open(m_device_name);

	m_serial.set_option(::boost::asio::serial_port_base::baud_rate(m_speed));
	m_serial.set_option(::boost::asio::serial_port_base::character_size(m_character_size));
	m_serial.set_option(::boost::asio::serial_port_base::parity(m_parity));
	m_serial.set_option(
			::boost::asio::serial_port_base::stop_bits(m_stop_bit));

}

void dstools::SerialPortBoost::close() {
	if (m_serial.is_open()) {
		m_serial.close();
	}
}

size_t dstools::SerialPortBoost::write(const byte_array &data) {
	std::lock_guard<std::mutex> guard_write(m_serial_port_write_mutex);
	if (m_serial.is_open()) {
		return m_serial.write_some(::boost::asio::buffer(data));
	} else {
		return 0;
	}
}

size_t dstools::SerialPortBoost::read(byte_array &data, size_t length) {
	std::lock_guard<std::mutex> guard_read(m_serial_port_read_mutex);
	if (m_serial.is_open()) {
		return m_reader.read(data, length);
	} else {
		return 0;
	}
}

void dstools::SerialPortBoost::set_device(const std::string &device_name) {
	std::lock_guard<std::mutex> guard_read(m_serial_port_read_mutex);
	std::lock_guard<std::mutex> guard_write(m_serial_port_write_mutex);
	this->m_device_name = device_name;
}

bool dstools::SerialPortBoost::is_open() {
	return m_serial.is_open();
}

void dstools::SerialPortBoost::set_speed(unsigned int speed) {
	std::lock_guard<std::mutex> guard_read(m_serial_port_read_mutex);
	std::lock_guard<std::mutex> guard_write(m_serial_port_write_mutex);
	this->m_speed = speed;
}

void dstools::SerialPortBoost::set_timeout(size_t timeout) {
	std::lock_guard<std::mutex> guard_read(m_serial_port_read_mutex);
	std::lock_guard<std::mutex> guard_write(m_serial_port_write_mutex);
	this->m_reader.setTimeout(timeout);
}

void dstools::SerialPortBoost::set_stop_bits(::boost::asio::serial_port_base::stop_bits::type p_stop_bit) {
	this->m_stop_bit = p_stop_bit;
}

void dstools::SerialPortBoost::set_parity(::boost::asio::serial_port_base::parity::type p_parity) {
	this->m_parity = p_parity;
}

void dstools::SerialPortBoost::set_character_size(unsigned int p_character_size) {
	this->m_character_size = p_character_size;
}

