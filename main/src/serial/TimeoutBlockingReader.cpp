/**
 *
 *  Copyright (C) 2019  Damian Sieradzki <damian.sieradzki@hotmail.com>
 *
 *  This file is part of DSTools.
 *
 *  DSTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DSTools.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include <serial/TimeoutBlockingReader.hpp>

dstools::boost::TimeoutBlockingReader::TimeoutBlockingReader(::boost::asio::serial_port &port, size_t timeout) :
		port(port),
		timeout(timeout),
		timer(port.get_io_service()) {

}

size_t dstools::boost::TimeoutBlockingReader::read(dstools::byte_array &data, size_t length) {

	auto val = new unsigned char;
	memset(val, '\0', length);

	// After a timeout & cancel it seems we need
	// to do a reset for subsequent reads to work.
	port.get_io_service().reset();

	// Asynchronously read 1 character.
	::boost::asio::async_read(port,
	                          ::boost::asio::buffer(val, length),
	                          ::boost::bind(&TimeoutBlockingReader::read_complete,
	                                        this,
	                                        ::boost::asio::placeholders::error,
	                                        ::boost::asio::placeholders::bytes_transferred));

	// Setup a deadline time to implement our timeout.
	if (timeout > 0) {
		timer.expires_from_now(::boost::posix_time::milliseconds(timeout));
	}
	timer.async_wait(::boost::bind(&TimeoutBlockingReader::time_out,
	                               this,
	                               ::boost::asio::placeholders::error));

	// This will block until a character is read
	// or until the it is cancelled.
	port.get_io_service().run();

	for (std::size_t i = 0; i < strlen((char *) val); i++) {
		data.emplace_back(static_cast<byte>(val[i]));
	}

	return bytes_transferred;
}

void dstools::boost::TimeoutBlockingReader::setTimeout(size_t timeout) {
	this->timeout = timeout;
}

void dstools::boost::TimeoutBlockingReader::time_out(const ::boost::system::error_code &error) {
	// Was the timeout was cancelled?
	if (error) {
		// yes
		return;
	}

	// no, we have timed out, so kill
	// the read operation
	// The read callback will be called
	// with an error
	port.cancel();
}

void dstools::boost::TimeoutBlockingReader::read_complete(const ::boost::system::error_code &error, size_t bytes_transferred) {

	this->bytes_transferred = (error) ? 0 : bytes_transferred;

	// Read has finished, so cancel the
	// timer.
	timer.cancel();
	if (port.is_open()) {
		port.cancel();
	}
}
