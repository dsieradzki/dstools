/**
 *
 *  Copyright (C) 2019  Damian Sieradzki <damian.sieradzki@hotmail.com>
 *
 *  This file is part of DSTools.
 *
 *  DSTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DSTools.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#ifndef DSTOOLS_LOGGER_HPP
#define DSTOOLS_LOGGER_HPP

#include <string>

namespace dstools {
	namespace logger {
		enum level {
			LOG_DEBUG, LOG_INFO, LOG_WARNING, LOG_ERROR
		};
	}

	class Logger {
	public:
		virtual ~Logger() = default;

		virtual void debug(const std::string &message) = 0;
		virtual void info(const std::string &message) = 0;
		virtual void warning(const std::string &message) = 0;
		virtual void error(const std::string &message) = 0;

		virtual void debug(const std::initializer_list<std::string> &messages) = 0;
		virtual void info(const std::initializer_list<std::string> &messages) = 0;
		virtual void warning(const std::initializer_list<std::string> &messages) = 0;
		virtual void error(const std::initializer_list<std::string> &messages) = 0;

		virtual void set_level(dstools::logger::level) = 0;
		virtual dstools::logger::level get_level() = 0;
	};
}


#endif
