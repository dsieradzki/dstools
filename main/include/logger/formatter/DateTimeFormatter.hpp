/**
 *
 *  Copyright (C) 2019  Damian Sieradzki <damian.sieradzki@hotmail.com>
 *
 *  This file is part of DSTools.
 *
 *  DSTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DSTools.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#ifndef DSTOOLS_DATETIMEFORMATTER_HPP
#define DSTOOLS_DATETIMEFORMATTER_HPP

#include <logger/formatter/Formatter.hpp>
#include <ctime>

namespace dstools {
	namespace logger {
		class DateTimeFormatter : public Formatter {
		public:
			std::string format(const std::string &msg) override;
		private:
			const std::string DATE_TIME_FORMAT = "%d-%m-%Y %H:%M:%S";
			std::string generate_date_time();
			std::string date_to_string(std::tm date_time);
		};
	}
}
#endif
