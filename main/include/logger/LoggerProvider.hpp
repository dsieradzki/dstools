/**
 *
 *  Copyright (C) 2019  Damian Sieradzki <damian.sieradzki@hotmail.com>
 *
 *  This file is part of DSTools.
 *
 *  DSTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DSTools.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#ifndef DSTOOLS_LOGGERPROVIDER_HPP
#define DSTOOLS_LOGGERPROVIDER_HPP

#include <logger/Logger.hpp>
#include <logger/appender/ConsoleAppender.hpp>
#include <logger/appender/FileAppender.hpp>
#include <logger/formatter/DateTimeFormatter.hpp>

#include <memory>
#include <mutex>
#include <vector>

#ifdef DSTOOLS_LOGS_DISABLED
#define LOG_DEBUG(...)
#define LOG_INFO(...)
#define LOG_WARNING(...)
#define LOG_ERROR(...)
#else
#define LOG_DEBUG(...) DSTOOLS_LOGGER->debug(__VA_ARGS__)
#define LOG_INFO(...) DSTOOLS_LOGGER->info(__VA_ARGS__)
#define LOG_WARNING(...) DSTOOLS_LOGGER->warning(__VA_ARGS__)
#define LOG_ERROR(...) DSTOOLS_LOGGER->error(__VA_ARGS__)
#endif


#define DSTOOLS_LOGGER dstools::LoggerProvider::get_logger()

namespace dstools {
	class LoggerProvider {
	public:
		static Logger *get_logger();
		static void set_level(dstools::logger::level level);
		static void setup_logger(std::unique_ptr<logger::Formatter> formatter = std::unique_ptr<logger::DateTimeFormatter>(new logger::DateTimeFormatter()),
		                         std::unique_ptr<logger::Appender> appender = std::unique_ptr<logger::ConsoleAppender>(new logger::ConsoleAppender()));

		static void setup_logger(std::unique_ptr<logger::Formatter> formatter, std::vector<std::unique_ptr<logger::Appender>> appenders);

	private:
		static std::unique_ptr<Logger> logger;
	};
}
#endif
