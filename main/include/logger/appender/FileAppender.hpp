/**
 *
 *  Copyright (C) 2019  Damian Sieradzki <damian.sieradzki@hotmail.com>
 *
 *  This file is part of DSTools.
 *
 *  DSTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DSTools.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#ifndef DSTOOLS_FILELOGAPPENDER_HPP
#define DSTOOLS_FILELOGAPPENDER_HPP

#include <logger/appender/Appender.hpp>
#include <fstream>

namespace dstools {
	namespace logger {
		class FileAppender : public logger::Appender {
		public:
			explicit FileAppender(const std::string &p_log_file_name);

			~FileAppender() override;

			void append(const std::string &msg) override;

		private:
			std::ofstream log_file;
			std::string log_file_name;
		};
	}
}

#endif
