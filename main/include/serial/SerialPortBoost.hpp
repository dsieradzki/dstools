/**
 *
 *  Copyright (C) 2019  Damian Sieradzki <damian.sieradzki@hotmail.com>
 *
 *  This file is part of DSTools.
 *
 *  DSTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DSTools.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#ifndef DSTOOLS_SERIALPORTBOOSTIMPL_HPP
#define DSTOOLS_SERIALPORTBOOSTIMPL_HPP

#include <serial/SerialPort.hpp>
#include <serial/TimeoutBlockingReader.hpp>

#include <boost/asio.hpp>
#include <mutex>

namespace dstools {

	class SerialPortBoost : public SerialPort {
	public:
		explicit SerialPortBoost();

		void open() override;

		void close() override;

		size_t write(const byte_array &data) override;

		size_t read(byte_array &data, size_t length) override;

		void set_device(const std::string &device_name) override;

		bool is_open() override;

		void set_speed(unsigned int speed) override;

		void set_timeout(size_t timeout) override;

		void set_stop_bits(::boost::asio::serial_port_base::stop_bits::type p_stop_bit) override;

		void set_parity(::boost::asio::serial_port_base::parity::type p_parity) override;

		void set_character_size(unsigned int p_character_size) override;


	private:
		std::mutex m_serial_port_read_mutex;
		std::mutex m_serial_port_write_mutex;
		std::string m_device_name;

		unsigned int m_speed;
		::boost::asio::io_service m_io;
		::boost::asio::serial_port m_serial;
		dstools::boost::TimeoutBlockingReader m_reader;

		::boost::asio::serial_port_base::stop_bits::type m_stop_bit = ::boost::asio::serial_port_base::stop_bits::type::one;
		::boost::asio::serial_port_base::parity::type m_parity = ::boost::asio::serial_port_base::parity::type::none;
		unsigned int m_character_size = 8;
	};
}

#endif
