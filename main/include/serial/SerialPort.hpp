/**
 *
 *  Copyright (C) 2019  Damian Sieradzki <damian.sieradzki@hotmail.com>
 *
 *  This file is part of DSTools.
 *
 *  DSTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DSTools.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#ifndef DSTOOLS_SERIALPORT_HPP
#define DSTOOLS_SERIALPORT_HPP

#include <string>
#include <boost/asio.hpp>

namespace dstools {
	typedef unsigned char byte;
	typedef std::vector<byte> byte_array;
}

namespace dstools {
	class SerialPort {
	public:
		virtual ~SerialPort() = default;

		virtual void set_device(const std::string &device_name) = 0;
		virtual void set_speed(unsigned int speed) = 0;

		virtual void open() = 0;
		virtual bool is_open() = 0;
		virtual void close() = 0;

		virtual size_t write(const byte_array &data) = 0;
		virtual size_t read(byte_array &data, size_t length) = 0;

		virtual void set_timeout(size_t timeout) = 0;
		virtual void set_stop_bits(::boost::asio::serial_port_base::stop_bits::type p_stop_bit) = 0;
		virtual void set_parity(::boost::asio::serial_port_base::parity::type p_parity) = 0;
		virtual void set_character_size(unsigned int p_character_size) = 0;

		static const int NO_TIMEOUT = 0;
	};
}

#endif
