/**
 *
 *  Copyright (C) 2019  Damian Sieradzki <damian.sieradzki@hotmail.com>
 *
 *  This file is part of DSTools.
 *
 *  DSTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DSTools.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#ifndef DSTOOLS_BLOCKING_READER_HPP
#define DSTOOLS_BLOCKING_READER_HPP

#include <serial/SerialPort.hpp>

#include <boost/asio/serial_port.hpp>
#include <boost/bind.hpp>
#include <boost/asio.hpp>

namespace dstools {
	namespace boost {
		class TimeoutBlockingReader {
		public:
			TimeoutBlockingReader(::boost::asio::serial_port &port, size_t timeout);

			// Reads a character or times out
			// returns false if the read times out
			size_t read(byte_array &data, size_t length);
			void setTimeout(size_t timeout);

		private:
			::boost::asio::serial_port &port;
			size_t timeout;
			::boost::asio::deadline_timer timer;
			size_t bytes_transferred;

			void time_out(const ::boost::system::error_code &error);
			// Called when an async read completes or has been cancelled
			void read_complete(const ::boost::system::error_code &error,
			                   size_t bytes_transferred);
		};
	}
}

#endif
