/**
 *
 *  Copyright (C) 2019  Damian Sieradzki <damian.sieradzki@hotmail.com>
 *
 *  This file is part of DSTools.
 *
 *  DSTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DSTools.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#ifndef DSTOOLS_ARGUMENTS_HPP
#define DSTOOLS_ARGUMENTS_HPP

#include <map>
#include <vector>
#include <string>

namespace dstools {
	class ArgumentParseException : public std::runtime_error {
	public:
		ArgumentParseException() : std::runtime_error("Argument parse exception") {};
	};

	class Arguments final {
	public:
		Arguments(int &argc, char *argv[]) noexcept(false);

		Arguments(const Arguments &arguments) = delete;

		~Arguments() = default;

		std::string argument_value(const std::string &key) const;
		std::string argument_value(const std::string &key, const std::string &default_value) const;
		bool is_present(const std::string &name) const;

		unsigned long count() const;

		std::string get_command() const;

		bool empty() const;

	private:
		std::map<std::string, std::string> value_arguments;
		std::vector<std::string> flag_arguments;
		std::string command;

		bool is_param(const std::string &value) const;

		bool is_flag_parameter(std::vector<std::string> args, unsigned int i) const;

		bool with_value_parameter(std::vector<std::string> args, unsigned int i) const;


		const std::string PARAM_PREFIX = "-";
		const std::string PARAM_PREFIX_2 = "--";

		bool is_command(std::basic_string<char> &basic_string, unsigned int i);
	};
}
#endif