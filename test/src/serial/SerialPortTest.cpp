/**
 *
 *  Copyright (C) 2019  Damian Sieradzki <damian.sieradzki@hotmail.com>
 *
 *  This file is part of DSTools.
 *
 *  DSTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DSTools.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "../../libs/catch.hpp"

#include <serial/SerialPort.hpp>
#include <serial/SerialPortBoost.hpp>

SCENARIO("Serial port ", "[SerialPort]") {

	GIVEN("SerialPort") {
		dstools::SerialPort *serial_port = new dstools::SerialPortBoost();

		WHEN("Get serialport") {
			THEN("SerialPort should be not null") {
				REQUIRE(serial_port);
			}
		}
	}
}