/**
 *
 *  Copyright (C) 2019  Damian Sieradzki <damian.sieradzki@hotmail.com>
 *
 *  This file is part of DSTools.
 *
 *  DSTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DSTools.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file

#include "../../libs/catch.hpp"

#include <arguments/Arguments.hpp>

#define _(STR) (const_cast<char *>(STR))

TEST_CASE("Argument should return 0", "[ArgumentsTest]") {
	int argc = 0;
	char *argv_i = new char;
	char **argv = &argv_i;
	dstools::Arguments arguments(argc, argv);

	REQUIRE(arguments.count() == 0);
	REQUIRE(arguments.empty());
}


TEST_CASE("Should parse one command and one flag", "[ArgumentsTest]") {
	int argc = 2;
	char *argv[] = {_("prog_name"), _("--flagArg")};
	dstools::Arguments arguments(argc, argv);

	REQUIRE(arguments.count() == 1);
	REQUIRE(arguments.is_present("--flagArg"));
	REQUIRE(arguments.argument_value("--flagArg").empty());
	REQUIRE_FALSE(arguments.is_present("--noExistingParam"));
}

TEST_CASE("Should return default value is flag is not preset", "[ArgumentsTest]") {
	int argc = 3;
	char *argv[] = {_("prog_name"), _("--paramArg"), _("value")};

	dstools::Arguments arguments(argc, argv);


	REQUIRE(arguments.argument_value("--paramArg") == "value");
	REQUIRE(arguments.argument_value("--paramArg", "def_val") == "value");
	REQUIRE(arguments.argument_value("--flagArgNotExists", "def_val") == "def_val");
}

TEST_CASE("Should parse one value argument", "[ArgumentsTest]") {
	int argc = 3;
	char *argv[] = {_("prog_name"), _("--paramArg"), _("value")};

	dstools::Arguments arguments(argc, argv);

	REQUIRE(arguments.count() == 1);
	REQUIRE(arguments.is_present("--paramArg"));
	REQUIRE(arguments.argument_value("--paramArg") == "value");
	REQUIRE_FALSE(arguments.is_present("--noExistingParam"));
}

TEST_CASE("Should parse one flag argument with one value argument", "[ArgumentsTest]") {
	int argc = 4;
	char *argv[] = {_("prog_name"), _("--flagArg"), _("--paramArg"), _("value")};

	dstools::Arguments arguments(argc, argv);

	REQUIRE(arguments.count() == 2);
	REQUIRE(arguments.is_present("--paramArg"));
	REQUIRE(arguments.is_present("--flagArg"));
	REQUIRE(arguments.argument_value("--paramArg") == "value");
	REQUIRE(arguments.argument_value("--flagArg").empty());
	REQUIRE_FALSE(arguments.is_present("--noExistingParam"));
}

TEST_CASE("Should parse one value argument with flag argument ", "[ArgumentsTest]") {
	int argc = 4;
	char *argv[] = {_("prog_name"), _("--paramArg"), _("value"), _("--flagArg")};

	dstools::Arguments arguments(argc, argv);

	REQUIRE(arguments.count() == 2);
	REQUIRE(arguments.is_present("--paramArg"));
	REQUIRE(arguments.is_present("--flagArg"));
	REQUIRE(arguments.argument_value("--paramArg") == "value");
	REQUIRE(arguments.argument_value("--flagArg").empty());
	REQUIRE_FALSE(arguments.is_present("--noExistingParam"));
}

TEST_CASE("Should parse one value argument with two flag argument ", "[ArgumentsTest]") {
	int argc = 5;
	char *argv[] = {_("prog_name"), _("--paramArg"), _("value"), _("--flagArg"), _("--flagArg2")};

	dstools::Arguments arguments(argc, argv);

	REQUIRE(arguments.count() == 3);
	REQUIRE(arguments.is_present("--paramArg"));
	REQUIRE(arguments.is_present("--flagArg"));
	REQUIRE(arguments.is_present("--flagArg2"));
	REQUIRE(arguments.argument_value("--paramArg") == "value");
	REQUIRE(arguments.argument_value("--flagArg").empty());
	REQUIRE(arguments.argument_value("--flagArg2").empty());
	REQUIRE_FALSE(arguments.is_present("--noExistingParam"));
}

TEST_CASE("Should parse two value argument with two flag argument between value argument", "[ArgumentsTest]") {
	int argc = 7;
	char *argv[] = {_("prog_name"), _("--paramArg"), _("value"), _("--flagArg"), _("--paramArg2"), _("value2"), _("--flagArg2")};

	dstools::Arguments arguments(argc, argv);

	REQUIRE(arguments.count() == 4);
	REQUIRE(arguments.is_present("--paramArg"));
	REQUIRE(arguments.is_present("--paramArg2"));
	REQUIRE(arguments.is_present("--flagArg"));
	REQUIRE(arguments.is_present("--flagArg2"));
	REQUIRE(arguments.argument_value("--paramArg") == "value");
	REQUIRE(arguments.argument_value("--paramArg2") == "value2");
	REQUIRE(arguments.argument_value("--flagArg").empty());
	REQUIRE(arguments.argument_value("--flagArg2").empty());
	REQUIRE_FALSE(arguments.is_present("--noExistingParam"));
}

TEST_CASE("Should get command", "[ArgumentsTest]") {
	int argc = 2;
	char *argv[] = {_("prog_name"), _("add")};

	dstools::Arguments arguments(argc, argv);

	REQUIRE(arguments.count() == 1);
	REQUIRE_FALSE(arguments.get_command().empty());
	REQUIRE_FALSE(arguments.is_present("--noExistingParam"));
}


TEST_CASE("Should get command with flag argument", "[ArgumentsTest]") {
	int argc = 3;
	char *argv[] = {_("prog_name"), _("add"), _("--flagArg")};

	dstools::Arguments arguments(argc, argv);

	REQUIRE(arguments.count() == 2);
	REQUIRE_FALSE(arguments.get_command().empty());
	REQUIRE(arguments.is_present("--flagArg"));
	REQUIRE_FALSE(arguments.is_present("--noExistingParam"));
}

TEST_CASE("Should get command with value argument", "[ArgumentsTest]") {
	int argc = 4;
	char *argv[] = {_("prog_name"), _("add"), _("--paramArg"), _("value")};

	dstools::Arguments arguments(argc, argv);

	REQUIRE(arguments.count() == 2);
	REQUIRE_FALSE(arguments.get_command().empty());
	REQUIRE(arguments.is_present("--paramArg"));
	REQUIRE(arguments.argument_value("--paramArg") == "value");
	REQUIRE_FALSE(arguments.is_present("--noExistingParam"));
}

TEST_CASE("Should get command with flag argument and value argument", "[ArgumentsTest]") {
	int argc = 5;
	char *argv[] = {_("prog_name"), _("add"), _("--paramArg"), _("value"), _("--flagArgument")};

	dstools::Arguments arguments(argc, argv);

	REQUIRE(arguments.count() == 3);
	REQUIRE_FALSE(arguments.get_command().empty());
	REQUIRE(arguments.is_present("--flagArgument"));
	REQUIRE(arguments.is_present("--paramArg"));
	REQUIRE(arguments.argument_value("--paramArg") == "value");
	REQUIRE_FALSE(arguments.is_present("--noExistingParam"));
}


// WRONG CASES
TEST_CASE("Should throw exception when will be only -", "[ArgumentsTest]") {
	int argc = 2;
	char *argv[] = {_("prog_name"), _("-")};

	REQUIRE_THROWS(dstools::Arguments(argc, argv));
}

TEST_CASE("Should throw exception when will be only --", "[ArgumentsTest]") {
	int argc = 2;
	char *argv[] = {_("prog_name"), _("--")};

	REQUIRE_THROWS(dstools::Arguments(argc, argv));
}

TEST_CASE("Should throw exception when words are between params - only one word can be exists as first arg and it is command", "[ArgumentsTest]") {
	int argc = 5;
	char *argv[] = {_("prog_name"), _("add"), _("--paramArg"), _("value"), _("word")};

	REQUIRE_THROWS(dstools::Arguments(argc, argv));
}