/**
 *
 *  Copyright (C) 2019  Damian Sieradzki <damian.sieradzki@hotmail.com>
 *
 *  This file is part of DSTools.
 *
 *  DSTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DSTools.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file

#include "../../libs/catch.hpp"

#include <logger/LoggerProvider.hpp>


class FakeFileAppender : public dstools::logger::Appender {
public:
	void append(const std::string &msg) override {
		log.append(msg);
	}

	std::string get_log() {
		return log;
	}

private:
	std::string log;
};


TEST_CASE("Logger should work with dynamic amount of parameters", "[Logger]") {

	auto mock_appender = new FakeFileAppender();

	dstools::LoggerProvider::setup_logger(
			std::unique_ptr<dstools::logger::DateTimeFormatter>(new dstools::logger::DateTimeFormatter()),
			std::unique_ptr<dstools::logger::Appender>(mock_appender)
	);

	LOG_ERROR({ "first", "second" });

	REQUIRE(mock_appender->get_log().find("firstsecond") != std::string::npos);
}


TEST_CASE("Logger should work with many appenders", "[LOGGER]") {
	auto mock_appender = new FakeFileAppender();
	auto second_mock_appender = new FakeFileAppender();

	std::vector<std::unique_ptr<dstools::logger::Appender>> appenders;
	appenders.emplace_back(std::unique_ptr<dstools::logger::Appender>(mock_appender));
	appenders.emplace_back(std::unique_ptr<dstools::logger::Appender>(second_mock_appender));


	dstools::LoggerProvider::setup_logger(std::unique_ptr<dstools::logger::DateTimeFormatter>(new dstools::logger::DateTimeFormatter()), std::move(appenders));

	LOG_ERROR({ "first", "second" });

	REQUIRE(mock_appender->get_log().find("firstsecond") != std::string::npos);
	REQUIRE(second_mock_appender->get_log().find("firstsecond") != std::string::npos);
}
